import React from 'react';

import { Product } from './components/Product/Product';
import { About } from './components/About/About';
import { Dignity } from './components/Dignity/Dignity';
import { Screenshots } from './components/Screenshoots/Screenshots';
import { Reviews } from './components/Reviews/Reviews';
import { Buy } from './components/Buy/Buy';
import { Footer } from './components/Footer/Footer';

const App = () => {
  return (
      <div className="App">
        <Product />
        <About />
        <Dignity />
        <Screenshots />
        <Reviews />
        <Buy />
        <Footer />
      </div>
  );
};

export default App;
