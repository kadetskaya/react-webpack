import React from 'react';
import renderer from 'react-test-renderer';
import {Screenshots} from '../components/Screenshoots/Screenshots';

describe('testing block Screenshots', () => {
    it('renders correctly', () => {
        const tree = renderer
            .create(<Screenshots/>)
            .toJSON();
        expect(tree).toMatchSnapshot();
    })
});

