import React from 'react';
import renderer from 'react-test-renderer';
import {BuyStandart} from '../components/Buy/BuyStandart';

describe('testing block Buy-standart', () => {
    it('renders correctly', () => {
        const tree = renderer
            .create(<BuyStandart/>)
            .toJSON();
        expect(tree).toMatchSnapshot();
    })
});

