import React from 'react';
import renderer from 'react-test-renderer';
import {Reviews} from '../components/Reviews/Reviews';

describe('testing block Reviews', () => {
    it('renders correctly', () => {
        const tree = renderer
            .create(<Reviews/>)
            .toJSON();
        expect(tree).toMatchSnapshot();
    })
});

