import React from 'react';
import renderer from 'react-test-renderer';
import {Dignity} from '../components/Dignity/Dignity';

describe('testing block Dignity', () => {
    it('renders correctly', () => {
        const tree = renderer
            .create(<Dignity/>)
            .toJSON();
        expect(tree).toMatchSnapshot();
    })
});

