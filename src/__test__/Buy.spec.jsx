import React from 'react';
import renderer from 'react-test-renderer';
import {Buy} from '../components/Buy/Buy';

describe('testing block Buy', () => {
    it('renders correctly', () => {
        const tree = renderer
            .create(<Buy/>)
            .toJSON();
        expect(tree).toMatchSnapshot();
    })
});

