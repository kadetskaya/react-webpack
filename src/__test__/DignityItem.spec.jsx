import React from 'react';
import renderer from 'react-test-renderer';
import {DignityItem} from '../components/Dignity/DignityItem';

describe('testing block Dignity-item', () => {
    it('renders correctly', () => {
        const tree = renderer
            .create(<DignityItem/>)
            .toJSON();
        expect(tree).toMatchSnapshot();
    })
});

