import React from 'react';
import renderer from 'react-test-renderer';
import {AboutImage} from '../components/About/AboutImage';

describe('testing block About-image', () => {
    it('renders correctly', () => {
        const tree = renderer
            .create(<AboutImage/>)
            .toJSON();
        expect(tree).toMatchSnapshot();
    })
});