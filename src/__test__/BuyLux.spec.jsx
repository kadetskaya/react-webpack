import React from 'react';
import renderer from 'react-test-renderer';
import {BuyLux} from '../components/Buy/BuyLux';

describe('testing block Buy-lux', () => {
    it('renders correctly', () => {
        const tree = renderer
            .create(<BuyLux/>)
            .toJSON();
        expect(tree).toMatchSnapshot();
    })
});

