import React from 'react';
import renderer from 'react-test-renderer';
import {ScreenshotItem} from '../components/Screenshoots/ScreenshotItem';

describe('testing block Screenshot-item', () => {
    it('renders correctly', () => {
        const tree = renderer
            .create(<ScreenshotItem/>)
            .toJSON();
        expect(tree).toMatchSnapshot();
    })
});

