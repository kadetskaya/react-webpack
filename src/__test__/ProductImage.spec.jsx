import React from 'react';
import renderer from 'react-test-renderer';
import {ProductImage} from '../components/Product/ProductImage';

describe('testing block Product-image', () => {
    it('renders correctly', () => {
        const tree = renderer
            .create(<ProductImage/>)
            .toJSON();
        expect(tree).toMatchSnapshot();
    })
});