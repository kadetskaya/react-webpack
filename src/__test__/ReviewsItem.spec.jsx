import React from 'react';
import renderer from 'react-test-renderer';
import {ReviewItem} from '../components/Reviews/ReveiwItem';

describe('testing block Reviews-item', () => {
    it('renders correctly', () => {
        const tree = renderer
            .create(<ReviewItem/>)
            .toJSON();
        expect(tree).toMatchSnapshot();
    })
});

