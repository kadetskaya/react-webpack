import React from 'react';
import renderer from 'react-test-renderer';
import {BuyPremium} from '../components/Buy/BuyPremium';

describe('testing block Buy-Premium', () => {
    it('renders correctly', () => {
        const tree = renderer
            .create(<BuyPremium/>)
            .toJSON();
        expect(tree).toMatchSnapshot();
    })
});

