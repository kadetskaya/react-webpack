import React from 'react';

const BuyLux = () => {
    return (
        <div className="buy-item-block">
            <h3 className="buy-item-title">Lux</h3>
            <p className="buy-item-price">$200</p>
            <ol className="buy-item-list">
                <li>Porro officia cumque sint deleniti;</li>
                <li>Тemo facere rem vitae odit;</li>
                <li>Cum odio, iste quia doloribus autem;</li>
                <li>Aperiam nulla ea neque;</li>
                <li>Porro officia cumque sint deleniti;</li>
                <li>Тemo facere rem vitae odit;</li>
                <li>Cum odio, iste quia doloribus autem;</li>
                <li>Aperiam nulla ea neque;</li>
                <li>Porro officia cumque sint deleniti;</li>
                <li>Тemo facere rem vitae odit;</li>
                <li>Cum odio, iste quia doloribus autem;</li>
                <li>Aperiam nulla ea neque.</li>
            </ol>
            <input type="button" value="BUY" className="buy-button" />
        </div>
    )
};

export { BuyLux };