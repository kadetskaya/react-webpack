import React from 'react';
import { BuyStandart } from './BuyStandart';
import { BuyPremium } from './BuyPremium';
import { BuyLux } from './BuyLux';

const Buy = () => {
    return (
        <section className="buy-block">
            <div className="container">
                <h2 className="buy-title">Buy  it now</h2>
                <div className="buy-items">
                    <BuyStandart />
                    <BuyPremium />
                    <BuyLux />
                </div>
            </div>
        </section>
    )
};

export {Buy};