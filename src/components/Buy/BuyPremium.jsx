import React from 'react';

const BuyPremium = () => {
    return (
        <div className="buy-item-block">
            <h3 className="buy-item-title">Premium</h3>
            <p className="buy-item-price">$150</p>
            <ol className="buy-item-list">
                <li>Porro officia cumque sint deleniti;</li>
                <li>Тemo facere rem vitae odit;</li>
                <li>Cum odio, iste quia doloribus autem;</li>
                <li>Aperiam nulla ea neque;</li>
                <li>Porro officia cumque sint deleniti;</li>
                <li>Тemo facere rem vitae odit;</li>
                <li>Cum odio, iste quia doloribus autem;</li>
                <li>Aperiam nulla ea neque.</li>
            </ol>
            <input type="button" value="BUY" className="buy-button" />
        </div>
    )
};

export { BuyPremium };