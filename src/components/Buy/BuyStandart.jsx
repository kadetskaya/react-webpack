import React from 'react';

const BuyStandart = () => {
    return (
        <div className="buy-item-block">
            <h3 className="buy-item-title">Standart</h3>
            <p className="buy-item-price">$100</p>
            <ol className="buy-item-list">
                <li>Porro officia cumque sint deleniti;</li>
                <li>Тemo facere rem vitae odit;</li>
                <li>Cum odio, iste quia doloribus autem;</li>
                <li>Aperiam nulla ea neque.</li>
            </ol>
            <input type="button" value="BUY" className="buy-button" />
        </div>
    )
};

export { BuyStandart };