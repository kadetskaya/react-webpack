import React from 'react';
import {ProductImage} from './ProductImage';

const Product = () => {
    return (
        <section className="product-block">
            <div className="container">
                <div className="product-info">
                    <div className="product-info-block">
                        <h1 className="product-info__title">Product name</h1>
                        <ul className="product-info__list">
                            <li><i className="fas fa-check product-info-list__icon"/>Put on this page information about your product</li>
                            <li><i className="fas fa-check product-info-list__icon"/>A detailed description of your product</li>
                            <li><i className="fas fa-check product-info-list__icon"/>Tell us about the advantages and merits</li>
                            <li><i className="fas fa-check product-info-list__icon"/>Associate the page with the payment system</li>
                        </ul>
                    </div>
                    <ProductImage/>
                </div>
            </div>
        </section>
    );
};

export { Product };