import React from 'react';
import { ScreenshotItem } from './ScreenshotItem';

const Screenshots = () => {
    return (
        <div className='scrinshots-block'>
            <div className='container'>
                <div className="scrinshots-info">
                    <h2 className="scrinshots-title">Scrinshots</h2>
                    <div className="scrinshot-block">
                        <ScreenshotItem text="Pariatur iure ab sunt nesciunt, quibusdam odio iste cumque itaque, ipsa
                            vel exercitationem ullam quos aut nostrum cupiditate fuga quaerat quam animi dolores.
                            Sequi itaque, unde perferendis nemo debitis dolor."
                        />
                        <ScreenshotItem text="Pariatur iure ab sunt nesciunt, quibusdam odio iste cumque itaque, ipsa
                            vel exercitationem ullam quos aut nostrum cupiditate fuga quaerat quam animi dolores.
                            Sequi itaque, unde perferendis nemo debitis dolor."
                        />

                        <ScreenshotItem text="Pariatur iure ab sunt nesciunt, quibusdam odio iste cumque itaque, ipsa
                            vel exercitationem ullam quos aut nostrum cupiditate fuga quaerat quam animi dolores.
                            Sequi itaque, unde perferendis nemo debitis dolor."
                        />
                        <ScreenshotItem text="Pariatur iure ab sunt nesciunt, quibusdam odio iste cumque itaque, ipsa
                            vel exercitationem ullam quos aut nostrum cupiditate fuga quaerat quam animi dolores.
                            Sequi itaque, unde perferendis nemo debitis dolor."
                        />
                    </div>
                </div>
            </div>
        </div>
    );
};

export { Screenshots };