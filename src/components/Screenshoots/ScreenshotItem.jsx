import React from 'react';

const ScreenshotItem = (props) => {
    return (
        <div className='scrinshots-item-block'>
            <div className='scrinshots-image'/>
            <div className="scrinshots-info-block">
                <h2 className="scrinshots-item__title">
                    The description for the image
                </h2>
                {/* eslint-disable-next-line react/destructuring-assignment,react/prop-types */}
                <div className="scrinshots-text">{props.text}</div>
            </div>
        </div>
    );
};

export { ScreenshotItem };
