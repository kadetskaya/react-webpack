import React from 'react';

const Footer = () => {
    return (
        <footer className="footer">
            <div className="container">
                <p className="footer_title">Contacts</p>
                <div className="footer_content">
                    <form action="" className="footer_form">
                        <input type="text" placeholder="Your name:" className="footer_input"/>
                        <input type="text" placeholder="Your email:" className="footer_input"/>
                        <textarea
                            name=""
                            id=""
                            cols="30"
                            rows="10"
                            placeholder="Your message:"
                            className="footer_input"
                        />
                        <input type="button" value="SEND" className="form_button"/>
                    </form>
                    <div className="feedback">
                        <div className="skype social">
                            <img className="feedback-icon" src="images/skype_icon.png" alt="skype"/>
                            <p className="social_description">here_your_login_skype</p>
                        </div>
                        <div className="icq social">
                            <img className="feedback-icon" src="images/icq_icon.png" alt="isq"/>
                            <p className="social_description">279679659</p>
                        </div>
                        <div className="email social">
                            <img className="feedback-icon" src="images/email_icon.png" alt="skype"/>
                            <p className="social_description">psdhtmlcss@mail.ru</p>
                        </div>
                        <div className="phone social">
                            <img className="feedback-icon" src="images/phone_icon.png" alt="skype"/>
                            <p className="social_description">80 00 4568 55 55</p>
                        </div>
                        <img src="images/social_buttons.png" alt="skype" className="social_icons"/>
                    </div>
                </div>
            </div>
        </footer>
    );
};

export {Footer};