import React from 'react';
import { DignityItem } from './DignityItem';

const Dignity = () => {
    return (
        <section className='dignity-block'>
            <div className='container'>
                <div className="dignity-info">
                    <h2 className="dignity-title">Dignity and pluses product</h2>
                    <div className="dignity-plus">
                            <DignityItem message={'Delectus dolorem vero quae beatae quasi dolor deserunt iste amet atque, ' +
                            'impedit iure placeat, ullam. Reprehenderit aliquam, nemo cum velit ratione perferendis quas, ' +
                            'maxime, quaerat porro totam, dolore minus inventore.'}/>
                            <DignityItem message={'Delectus dolorem vero quae beatae quasi dolor deserunt iste amet atque, ' +
                            'impedit iure placeat, ullam. Reprehenderit aliquam, nemo cum velit ratione perferendis quas, ' +
                            'maxime, quaerat porro totam, dolore minus inventore.'}/>
                            <DignityItem message={'Delectus dolorem vero quae beatae quasi dolor deserunt iste amet atque, ' +
                            'impedit iure placeat, ullam. Reprehenderit aliquam, nemo cum velit ratione perferendis quas, ' +
                            'maxime, quaerat porro totam, dolore minus inventore.'}/>

                            <DignityItem message={'Delectus dolorem vero quae beatae quasi dolor deserunt iste amet atque, ' +
                            'impedit iure placeat, ullam. Reprehenderit aliquam, nemo cum velit ratione perferendis quas, ' +
                            'maxime, quaerat porro totam, dolore minus inventore.'}/>
                            <DignityItem message={'Delectus dolorem vero quae beatae quasi dolor deserunt iste amet atque, ' +
                            'impedit iure placeat, ullam. Reprehenderit aliquam, nemo cum velit ratione perferendis quas, ' +
                            'maxime, quaerat porro totam, dolore minus inventore.'}/>
                            <DignityItem message={'Delectus dolorem vero quae beatae quasi dolor deserunt iste amet atque, ' +
                            'impedit iure placeat, ullam. Reprehenderit aliquam, nemo cum velit ratione perferendis quas, ' +
                            'maxime, quaerat porro totam, dolore minus inventore.'}/>

                    </div>
                </div>
            </div>
        </section>
    );
};

export { Dignity };

