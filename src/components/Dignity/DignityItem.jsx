import React from 'react';

const DignityItem = (props) => {
    return (
        <div className="dignity-item-block">
            <div className='dignity-icon'>
                <i className="fas fa-plus dignity-icon-plus"/>
            </div>
            <div className="dignity-item-description">{props.message}</div>
        </div>
    );
};

export { DignityItem };