import React from 'react';


const ReviewItem = (props) => {
    return (
        <div className="review-item-block">
            <div className="review-image"/>
            <p className="left" />
            <div className="review-description">
                <i className="review-text">{props.text}</i>
                <div className="review-author">{props.author}</div>
            </div>
        </div>
    )
};

export { ReviewItem };