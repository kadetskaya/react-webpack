import React from 'react';
import {ReviewItem} from './ReveiwItem';

const Reviews = () => {
    return (
        <section className="reviews-block">
            <div className="container">
                <h2 className="reviews-title">Reviews</h2>
                <div className="reviews-item">
                    <ReviewItem text="Porro officia cumque sint deleniti nemo facere rem vitae odit inventore cum odio,
                                iste quia doloribus autem aperiam nulla ea neque reprehenderit. Libero doloribus, possimus
                                officiis sapiente necessitatibus commodi consectetur?"
                                author="Lourens S."/>
                    <ReviewItem text="Porro officia cumque sint deleniti nemo facere rem vitae odit inventore cum odio,
                                iste quia doloribus autem aperiam nulla ea neque reprehenderit. Libero doloribus, possimus
                                officiis sapiente necessitatibus commodi consectetur?"
                                author="Lourens S."/>
                    <ReviewItem text="Porro officia cumque sint deleniti nemo facere rem vitae odit inventore cum odio,
                                iste quia doloribus autem aperiam nulla ea neque reprehenderit. Libero doloribus, possimus
                                officiis sapiente necessitatibus commodi consectetur?"
                                author="Lourens S."/>
                    <ReviewItem text="Porro officia cumque sint deleniti nemo facere rem vitae odit inventore cum odio,
                                iste quia doloribus autem aperiam nulla ea neque reprehenderit. Libero doloribus, possimus
                                officiis sapiente necessitatibus commodi consectetur?"
                                author="Lourens S."/>
                </div>
            </div>
        </section>
    )
};

export {Reviews};