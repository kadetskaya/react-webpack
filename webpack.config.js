const fs = require('fs');
const path = require('path');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const ROOT_DIR = fs.realpathSync(process.cwd());
const analyse = process.argv.includes('--analyse');

function pathResolve(...args) {
    return path.resolve(ROOT_DIR, ...args);
}

const isDev = process.env.NODE_ENV !== 'production';

function getName(ext) {
    return `[name].[${isDev ? 'hash:8' : 'contenthash'}].bundle.${ext}`
}

function getImageName(ext) {
    return `[name].[${isDev ? 'hash:8' : 'contenthash'}].${ext}`
}

module.exports = {
    mode: isDev ? 'development' : 'production',
    entry: {
        main: './src/index.jsx'
    },
    output: {
        filename: getName('js'),
        path: pathResolve('dist')
    },
    resolve: {
        extensions: ['.js', '.json', '.jsx']
    },
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                exclude: [/node_module/],
                use: ['babel-loader']
            },
            {
                test: /\.(css)$/,
                use: ['style-loader', 'css-loader']
            },
            {
                test: /\.s[ac]ss$/i,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    'sass-loader',
                ]
            },
            {
                test: /\.(png|jpg|svg|gif)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: `assets/${getImageName('[ext]')}`
                        }
                    }
                ]
            },
            {
                test: /\.(ttf|woff|woff2|eot)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            outputPath: 'fonts'
                        }
                    }
                ]
            }
        ]
    },
    plugins: [
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            template: pathResolve('public/index.html'),
            chunks: ['main']
        }),
        new MiniCssExtractPlugin({
            filename: '[name].css'
        }),
        new CopyWebpackPlugin([
            {
                from: pathResolve('src/assets/images'),
                to: pathResolve('dist/images')
            }
        ]),
        analyse && new BundleAnalyzerPlugin()
    ].filter(Boolean),
    devServer:
        {
            port: '3000',
            open:
                true
        }
};