module.exports = {
    env: {
        browser: true,
        es6: true,
        jest: true
    },
    extends: ['airbnb', 'prettier', 'prettier/react'],
    globals: {
        Atomics: 'readonly',
        SharedArrayBuffer: 'readonly',
    },
    parserOptions: {
        ecmaFeatures: {
            jsx: true,
        },
        ecmaVersion: 2018,
        sourceType: 'module',
    },
    plugins: ['react'],
    rules: {
        'quotes': [
            'error',
            'single'
        ],
        'import/prefer-default-export': 'off',
        'react/prop-types': 0,
        'react/destructuring-assignment': 'off'
    }
};